-- =============================================
-- File:        xurban65_xsolci00.sql
-- Authors:     Cyril Urban, Vit Solcik
-- date:        2017-04-28
-- Description: SQL skript pro databazi lekarny
-- =============================================

------------------------------------------------
----------------Smazani objektu-----------------
------------------------------------------------
-- smazani tabulek
DROP TABLE sklad CASCADE CONSTRAINTS;
DROP TABLE vydavany_lek CASCADE CONSTRAINTS;
DROP TABLE lek CASCADE CONSTRAINTS;
DROP TABLE zdravotni_pojistovna CASCADE CONSTRAINTS;
DROP TABLE prispevek CASCADE CONSTRAINTS;
-- smazani sekvence
DROP SEQUENCE auto_id_vydej;
-- smazani materializovaneho pohledu
DROP MATERIALIZED VIEW evidence_vydanych_leku;

------------------------------------------------
---------------Vytvoreni objektu----------------
------------------------------------------------
-- Vytvoreni tabulky lek
CREATE TABLE lek (
  id_lek INTEGER NOT NULL,
  je_na_predpis VARCHAR(3) NOT NULL,
  nazev VARCHAR(50) NOT NULL, 
  typ_leku VARCHAR(50) NOT NULL, 
  prodejni_cena INTEGER NOT NULL,
  zarucni_lhuta DATE,
  sklad INTEGER NOT NULL,
  je_prodan VARCHAR(3) NOT NULL,
  pojistovna_pripevek INTEGER
  );

-- Vytvoreni tabulky vydavany_lek
CREATE TABLE vydavany_lek (
  id_vydej INTEGER,
  prodavany_lek INTEGER NOT NULL,
  datum_vydani DATE,
  id_pojistovna INTEGER
  );

-- Vytvoreni tabulky sklad
CREATE TABLE sklad (
  id_sklad INTEGER NOT NULL,  
  adresa VARCHAR(50) NOT NULL, 
  skladova_plocha INTEGER NOT NULL
  );

-- Vytvoreni tabulky zdravotni_pojistovna
CREATE TABLE zdravotni_pojistovna (
  id_pojistovna INTEGER NOT NULL,  
  nazev_pojistovny VARCHAR(50) NOT NULL, 
  fakturacni_adresa VARCHAR(50) NOT NULL
  );

-- Vytvoreni tabulky prispevek
CREATE TABLE prispevek (
  prispovana_cena INTEGER NOT NULL, 
  pojistovna INTEGER NOT NULL, 
  nazev_leku VARCHAR(50) NOT NULL 
  );

------------------------------------------------
-------------Urceni primarnich klicu------------
------------------------------------------------  
ALTER TABLE lek ADD CONSTRAINT PK_lek PRIMARY KEY (id_lek);
ALTER TABLE vydavany_lek ADD  CONSTRAINT PK_vydavany_lek PRIMARY KEY (id_vydej);
ALTER TABLE sklad ADD CONSTRAINT PK_sklad PRIMARY KEY (id_sklad);
ALTER TABLE zdravotni_pojistovna ADD  CONSTRAINT PK_pojistovna PRIMARY KEY (id_pojistovna);
ALTER TABLE prispevek ADD CONSTRAINT PK_pojistovna_lek PRIMARY KEY (pojistovna, nazev_leku);

------------------------------------------------
--------------Urceni cizich klicu---------------
------------------------------------------------
ALTER TABLE vydavany_lek ADD CONSTRAINT FK_vydavany_lek FOREIGN KEY (prodavany_lek) REFERENCES lek;
ALTER TABLE lek ADD CONSTRAINT FK_sklad FOREIGN KEY (sklad) REFERENCES sklad;
ALTER TABLE prispevek ADD CONSTRAINT FK_pojistovna FOREIGN KEY (pojistovna) REFERENCES zdravotni_pojistovna;

------------------------------------------------
--------------------Triggery--------------------
------------------------------------------------
-- Trigger u leku, ktere jdou do sekce vydavany_lek nastavi hodnotu je_prodano
-- na ANO.
CREATE OR REPLACE TRIGGER prodano
  AFTER INSERT ON vydavany_lek
  FOR EACH ROW
BEGIN
  UPDATE lek SET je_prodan = 'ANO'
  WHERE id_lek = :NEW.prodavany_lek;
END prodano;
/

-- Tento triger automaticky vytvori ID (PK) pro kazdy novy vydej.
-- Navic vytvori pro kazdy vydej, automaticke casove razitko (datum vydani) */ 
CREATE SEQUENCE auto_id_vydej; -- pamatuji si hodnotu posledniho ID
CREATE OR REPLACE TRIGGER auto_id_tvoric
  BEFORE INSERT ON vydavany_lek
  FOR EACH ROW
BEGIN
  :NEW.id_vydej := auto_id_vydej.nextval; -- id_vydej = auto_id_vydej + 1
  :NEW.datum_vydani := SYSDATE; -- casove razitko
END auto_id_tvoric;
/

-- Tento triger je inspirovan filozofii tzv. Batovych cen. To znamena,
-- ze u vsech leku, ktera jsou nasobky desiti se jejich hodnota snizi o 1 korunu.
-- Napr.: 350 -> 349 Kc. Tato podminka plati u vyrobku s prodejni cenou alespon
-- 100 Kc. Kdyby s touto zmenou ceny uzivatel nesouhlasil, muze ji upravit zpet na
-- celou desetikorunu (triger je pouze pro INSERT nikoli i pro UPDATE).
CREATE OR REPLACE TRIGGER batovy_ceny
  BEFORE INSERT ON lek
  FOR EACH ROW
BEGIN
  UPDATE lek SET prodejni_cena = (prodejni_cena-1)
  WHERE ((MOD(prodejni_cena, 10) = 0) 
        AND (prodejni_cena > 99));
END batovy_ceny;
/

------------------------------------------------
------------Vlozeni referencnich dat------------
------------------------------------------------
-- Vlozeni referencnich dat do tabulky sklad
INSERT INTO sklad VALUES (0, 'Technicka 10, Brno', 328);
INSERT INTO sklad VALUES (1, 'Vysehradska 34, Praha', 685);
INSERT INTO sklad VALUES (2, 'Staropramenska 115, Ostrava', 290);

-- Vlozeni referencnich dat do tabulky lek
INSERT INTO lek VALUES (0, 'NE', 'Paralen', 'tablety', '100', TO_DATE('2018-01-31','YYYY-MM-DD'), 0 , 'NE', NULL);
INSERT INTO lek VALUES (1, 'NE', 'Orofar', 'pastilky', '135', TO_DATE('2018-05-11','YYYY-MM-DD'), 1 , 'NE', NULL);
INSERT INTO lek VALUES (2, 'NE', 'Clavin Platinum', 'tablety', '400', TO_DATE('2019-04-16','YYYY-MM-DD'), 0 , 'NE', NULL);
INSERT INTO lek VALUES (3, 'ANO', 'Olynth HA', 'nosni sprej', '118', TO_DATE('2017-09-18','YYYY-MM-DD'), 2 , 'NE', NULL);
INSERT INTO lek VALUES (4, 'NE', 'Ibalgin', 'tablety', '60', TO_DATE('2017-11-19','YYYY-MM-DD'), 0 , 'NE', NULL);
INSERT INTO lek VALUES (5, 'NE', 'Indulona', 'krem', '80', TO_DATE('2019-07-29','YYYY-MM-DD'), 1 , 'NE', NULL);
INSERT INTO lek VALUES (6, 'NE', 'Orofar', 'pastilky', '135', TO_DATE('2018-12-31','YYYY-MM-DD'), 0 , 'NE', NULL);

-- Vlozeni referencnich dat do tabulky vydavany_lek
INSERT INTO vydavany_lek VALUES (NULL, 0, NULL, 1);
INSERT INTO vydavany_lek VALUES (NULL, 1, NULL, 2);
INSERT INTO vydavany_lek VALUES (NULL, 2, NULL, 3);
INSERT INTO vydavany_lek VALUES (NULL, 3, NULL, 4);
INSERT INTO vydavany_lek VALUES (NULL, 6, NULL, 5);

-- Vlozeni referencnich dat do tabulky zdravotni_pojistovna
INSERT INTO zdravotni_pojistovna VALUES (0, 'VZP', 'Orlicka 2020, Praha');
INSERT INTO zdravotni_pojistovna VALUES (1, 'VOZP', 'Banskobystricka 40, Brno');
INSERT INTO zdravotni_pojistovna VALUES (2, 'CZVP', 'Jeremenkova 11, Ostrava');

-- Vlozeni referencnich dat do tabulky prispevek
INSERT INTO prispevek VALUES (49, 1, 'Paralen');
INSERT INTO prispevek VALUES (99, 2, 'Ibalgin');


------------------------------------------------
-----------------SELECT prikazy-----------------
------------------------------------------------
-- Propojeni dvou tabulek.
-- Vybere nazev leku, prodejni cenu a zda-li byl u vydani na predpis.
SELECT nazev, prodejni_cena, je_na_predpis
FROM vydavany_lek 
JOIN lek ON vydavany_lek.prodavany_lek=lek.id_lek;

-- Propojeni dvou tabulek.
-- Napise na jake adrese je lek uskladnen.
SELECT id_lek, nazev, adresa
FROM lek
JOIN sklad ON lek.sklad=sklad.id_sklad;

-- Propojeni tri tabulek.
-- Vypsani vsech prispevku - nazev leku, na ktery je dan prispevek pojistovny, jeho prodejni cena,
-- prispivana cena dane pojistovny a nazev pojistovny.
SELECT nazev, prodejni_cena, prispovana_cena, nazev_pojistovny
FROM prispevek 
JOIN lek ON prispevek.nazev_leku=lek.nazev
JOIN zdravotni_pojistovna ON prispevek.pojistovna=zdravotni_pojistovna.id_pojistovna;

-- Vypise nazev vydavanych leku a secte jejich cenu a pocet
SELECT lek.nazev, count(lek.nazev), sum(lek.prodejni_cena)
FROM vydavany_lek 
JOIN lek ON vydavany_lek.prodavany_lek=lek.id_lek
GROUP BY lek.nazev, lek.nazev, lek.prodejni_cena;

-- Vybere vsechny leky a vypise jejich celkovy pocet
SELECT nazev, count(nazev) FROM lek GROUP BY nazev;

-- Vybere vsechny leky na predpis drazsi nez 100
SELECT nazev, prodejni_cena
FROM lek
WHERE prodejni_cena > 100
  AND je_na_predpis IN ( SELECT je_na_predpis
        FROM lek
        WHERE je_na_predpis = 'ANO'
);

-- Vyhleda zda se nachazi Orofar na skladu id_sklad = 1, pokud ano vypise adresu skladu
SELECT adresa
FROM sklad
WHERE ID_SKLAD = 1 and EXISTS
(
          SELECT lek.nazev
          FROM lek
          JOIN sklad ON lek.sklad=sklad.id_sklad
          WHERE lek.nazev = 'Orofar' and sklad.id_sklad = 1
);

------------------------------------------------
-------------------PROCEDURY--------------------
------------------------------------------------
-- Procedura pro nalezeni daneho leku podle nazvu z parametru procedury.
SET serveroutput ON;
CREATE OR REPLACE PROCEDURE hledatLek(menoZ IN VARCHAR2)
IS CURSOR z IS
  SELECT id_lek, nazev from LEK;
  polozkaObsah z%ROWTYPE;
  found boolean;

BEGIN
  OPEN z;
  found := FALSE;
  LOOP
    FETCH z INTO polozkaObsah;
    EXIT WHEN z%NOTFOUND;
    IF (polozkaObsah.nazev = menoZ) THEN
      dbms_output.put_line('Nazev: ' || polozkaObsah.nazev ||'  ID:' || polozkaObsah.id_lek);
      found := TRUE;
    END IF;
  END LOOP;
  IF (found = FALSE) THEN
    dbms_output.put_line('Nenalezeno');
  END IF;
  CLOSE z;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    dbms_output.put_line('neexistuje!');
END;
/

-- Procedura pro vypsani uctenky.
SET serveroutput ON;
CREATE OR REPLACE PROCEDURE sumaNakup
IS CURSOR kurzor IS
  SELECT lek.nazev, count(lek.nazev) as cnt, sum(lek.prodejni_cena) as sumary
  FROM vydavany_lek 
  JOIN lek ON vydavany_lek.prodavany_lek=lek.id_lek
  GROUP BY lek.nazev, lek.nazev, lek.prodejni_cena;
  polozkaObsah kurzor%ROWTYPE;

BEGIN
  OPEN kurzor;
  LOOP
    FETCH kurzor INTO polozkaObsah;
    EXIT WHEN kurzor%NOTFOUND;
    dbms_output.put_line('Nazev: ' || polozkaObsah.nazev ||'  Pocet kusu:' || polozkaObsah.cnt || '  Cena:' || polozkaObsah.sumary);
  END LOOP;
  CLOSE kurzor;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    dbms_output.put_line('neexistuje!');
END;
/

-- Procedura pro pridni leku do vydavanych leku
CREATE OR REPLACE PROCEDURE pridejNaUctenku(
     p_vydej IN vydavany_lek.id_vydej%TYPE,
     p_vydavany_lek IN vydavany_lek.prodavany_lek%TYPE,
     p_datum_vydani IN vydavany_lek.datum_vydani%TYPE,
     p_id_pojistovna IN vydavany_lek.id_pojistovna%TYPE)
IS
BEGIN
  INSERT INTO vydavany_lek VALUES (p_vydej, p_vydavany_lek, p_datum_vydani, p_id_pojistovna);
  UPDATE lek
    SET pojistovna_pripevek = p_id_pojistovna
    WHERE id_lek = p_vydavany_lek;
  COMMIT;
END;
/


-- volani procedury a vypis tabulky vydavany_lek
exec pridejNaUctenku( NULL, 4, NULL, 1);
SELECT * from lek;
SELECT * FROM vydavany_lek;
-- volani dalsich procedur a kontrolni vypis
exec hledatLek('Paralen');
exec sumaNakup();
SELECT * FROM lek;

-- Procedura pro vymazani nakupu
SET serveroutput ON;
CREATE OR REPLACE PROCEDURE smazatUctenku
IS CURSOR z IS
 SELECT * 
 FROM lek 
 JOIN vydavany_lek ON  lek.id_lek = vydavany_lek.prodavany_lek
 FOR UPDATE OF lek.je_prodan;
 polozkaObsah z%ROWTYPE;
 BEGIN
 OPEN z;
 LOOP
    FETCH z INTO polozkaObsah;
    EXIT WHEN z%NOTFOUND;
    DELETE FROM vydavany_lek WHERE vydavany_lek.prodavany_lek = polozkaObsah.id_lek;
 END LOOP;
 CLOSE z;

EXCEPTION
 WHEN NO_DATA_FOUND THEN
   dbms_output.put_line('neexistuje!');
END;
/

-- volani procedury: ukazka leku pred a po vymazani
SELECT * FROM LEK;
exec smazatUctenku();
SELECT * FROM LEK;

-- procedura pro vypis prispevku pojistoven
SET serveroutput ON;
CREATE OR REPLACE PROCEDURE evidencePojistovna
IS CURSOR kurzor IS
  SELECT prispevek.prispovana_cena as sumary, prispevek.pojistovna as id_pojistovna
  FROM prispevek
  JOIN lek on prispevek.pojistovna = lek.pojistovna_pripevek;
  --GROUP BY prispevek.prispovana_cena, prispevek.pojistovna;
  polozkaObsah kurzor%ROWTYPE;
  jmeno VARCHAR(50);
BEGIN
  OPEN kurzor;
  LOOP
    FETCH kurzor INTO polozkaObsah;
    EXIT WHEN kurzor%NOTFOUND;
    SELECT zdravotni_pojistovna.nazev_pojistovny 
    INTO jmeno FROM zdravotni_pojistovna 
    WHERE zdravotni_pojistovna.id_pojistovna = polozkaObsah.id_pojistovna;
    dbms_output.put_line('Nazev: ' || jmeno ||'  ID: ' || polozkaObsah.id_pojistovna || '  Castka:' || polozkaObsah.sumary);
  END LOOP;
  CLOSE kurzor;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    dbms_output.put_line('neexistuje!');
END;
/

--vykonani provedury
exec evidencePojistovna;

------------------------------------------------
------------------EXPLAIN PLAN------------------
------------------------------------------------
EXPLAIN PLAN SET STATEMENT_ID = 'exampleExplainPlan' FOR
  SELECT lek.nazev, count(lek.nazev), sum(lek.prodejni_cena)
  FROM vydavany_lek 
  JOIN lek ON vydavany_lek.prodavany_lek=lek.id_lek
  GROUP BY lek.nazev, lek.nazev, lek.prodejni_cena;

SELECT plan_table_output FROM TABLE(dbms_xplan.display('plan_table','exampleExplainPlan','typical'));

CREATE INDEX index_1 ON lek(nazev);
CREATE INDEX index_2 ON vydavany_lek(prodavany_lek);

EXPLAIN PLAN SET STATEMENT_ID = 'exampleExplainPlan2' FOR
  SELECT lek.nazev, count(lek.nazev), sum(lek.prodejni_cena)
  FROM vydavany_lek 
  JOIN lek ON vydavany_lek.prodavany_lek=lek.id_lek
  GROUP BY lek.nazev, lek.nazev, lek.prodejni_cena;

SELECT plan_table_output FROM TABLE(dbms_xplan.display('plan_table','exampleExplainPlan2','typical'));

------------------------------------------------
--------------------OPRAVNENI-------------------
------------------------------------------------
-- tabulky
GRANT ALL ON sklad TO xsolci00;
GRANT ALL ON vydavany_lek TO xsolci00;
GRANT ALL ON lek TO xsolci00;
--sekvence
GRANT ALL ON auto_id_vydej TO xsolci00;
-- procedury
GRANT ALL ON hledatLek  TO xsolci00;
GRANT ALL ON sumaNakup TO xsolci00;
GRANT ALL ON smazatUctenku  TO xsolci00;
GRANT ALL ON pridejNaUctenku  TO xsolci00;

-- zakaz pristupu k tabulkam
REVOKE ALL ON zdravotni_pojistovna FROM xsolci00;
REVOKE ALL ON prispevek FROM xsolci00;

------------------------------------------------
------------Materializovany pohlad--------------
------------------------------------------------
-- Vytvoreni pohledu, kde jsou vybrany vsechny leky, 
-- ktere byly prodany (evidence vydanych leku).
CREATE MATERIALIZED VIEW LOG ON lek WITH PRIMARY KEY, ROWID(je_prodan) INCLUDING NEW VALUES; -- log pro pohled

CREATE MATERIALIZED VIEW evidence_vydanych_leku
  NOLOGGING
  CACHE
  BUILD IMMEDIATE
  REFRESH FAST ON COMMIT
  ENABLE QUERY REWRITE
  AS
    SELECT id_lek, je_na_predpis, nazev, prodejni_cena, typ_lek, zarucni_lhuta, je_prodan AS vydane_leky
    FROM LEK
    WHERE je_prodan = 'ANO';

--  prideleni prav k mat. pohledu
GRANT ALL ON evidence_vydanych_leku TO xsolci00;

-- Popis materializovaneho pohledu:
-- 1) nejdrive vypisu vse z mat. pohledu - evidenci vydanych leku (pro kontrolu)
SELECT * FROM evidence_vydanych_leku;
-- 2) pridam lek (s ID 4) do vydavany_lek, tim provedu novy nakup, automaticky se provede: lek s ID=4.je_prodano='ANO'
INSERT INTO vydavany_lek VALUES (NULL, 4, NULL, 8);
-- 3) v tabulce lek sice novy lek s atributem je_prodano='ANO' je, ale v materializovanem pohledu ne (overim vypsanim)
SELECT * FROM evidence_vydanych_leku;
-- 4) prikazem COMMIT aktualizuji mat. pohled - nyni je novy lek i v mat. pohledu (v evidenci vydanych leku)
COMMIT;
-- 5) overim korektnim vypisem
SELECT * FROM evidence_vydanych_leku;

